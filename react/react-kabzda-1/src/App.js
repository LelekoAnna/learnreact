import React, { Suspense, useEffect } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { initializeApp } from './data/app-reducer';
import { Route, withRouter } from 'react-router-dom'

import './App.css';

import Login from './components/Login/Login';
import NavbarContainer from './components/Navbar/NavbarContainer';
import HeaderContainer from './components/Header/HeaderContainer';
import ProfileContainer from './components/Profile/ProfileContainer';

const DialogsContainer = React.lazy(() => import('./components/Dialogs/DialogsContainer'));
const News = React.lazy(() => import('./components/News/News'));
const Music = React.lazy(() => import('./components/Music/Music'));
const UsersContainer = React.lazy(() => import('./components/Users/UsersContainer'));
const Settings = React.lazy(() => import('./components/Settings/Settings'));

const App = (props) => {
  useEffect(()=> {props.initializeApp()})

    return (
      <div className='app-wrapper'>
        <HeaderContainer/>
        <NavbarContainer/>
        <div className='content'>
          <Route path='/login' render={() => <Login/>}/>
          <Route path='/profile/:id?' render={() => <ProfileContainer/>}/>
          <Suspense fallback={<div>Loading...</div>}>
            <Route path='/dialogs' render={() => <DialogsContainer/>}/>
            <Route path='/news' component={News}/>
            <Route path='/music' component={Music}/>
            <Route path='/users' render={() => <UsersContainer/>}/>
            <Route path='/settings' component={Settings}/>
          </Suspense>
        </div>
      </div>
    );
}

const mapStateToProps = (state) => ({
  initialized: state.app.initialized
})

export default compose(
  withRouter,
  connect(mapStateToProps, { initializeApp }))(App);
