import React from 'react';
import { LoginFormContainer } from './LoginForm/LoginForm';
import { connect } from 'react-redux';
import { login } from '../../data/auth-reducer';
import { Redirect } from 'react-router-dom';


const Login = (props) => {
  const onSubmit = (formData) => {
    props.login(formData.login, formData.password, formData.rememberMe)
  }
  if (props.isAuth) {
    return <Redirect to='/profile'/>
  }
  return (
    <>
      <h1>Login</h1>
      <LoginFormContainer onSubmit={onSubmit}/>
    </>
  )
}

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth
})

export default connect(mapStateToProps, { login })(Login);
