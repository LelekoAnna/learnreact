import style from '../Login.module.css';
import { Field, reduxForm } from 'redux-form';
import { Input } from '../../common/FormsControl/FormsControl';
import React from 'react';
import { required } from '../../../utils/validators/validators';

const LoginForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div className={style.field}>
        <label htmlFor="login">Login</label>
        <Field name="login" component={Input} validate={[ required ]} placeholder='Enter your login'/>
      </div>
      <div className={style.field}>
        <label htmlFor="password">Password</label>
        <Field name="password" component={Input} validate={[ required ]} placeholder='Enter your password'
               type='password'/>
      </div>
      <div className={style.field}>
        <Field name="rememberMe" component={Input} type="checkbox"/> remember me
      </div>
      {props.error && <div className={style.error}>{props.error}</div>}
      <button>Submit</button>
    </form>
  )
}

export const LoginFormContainer = reduxForm({
  form: 'login'
})(LoginForm)
