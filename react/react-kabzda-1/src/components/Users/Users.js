import React from 'react';
import User from './User/User';
import Paginator from '../common/Paginator/Paginator';

const Users = (props) => {
  let userElement = props.state.map(user => <User key={user.id} user={user} getID={props.getID} disabledButtons={props.disabledButtons}/>)

  return (
    <div>
      <Paginator pagesCount={props.pagesCount} currentPage={props.currentPage} switchPage={props.switchPage} onPageChanged={props.onPageChanged}/>
      <ul>{userElement}</ul>
    </div>
  )
}

export default Users;
