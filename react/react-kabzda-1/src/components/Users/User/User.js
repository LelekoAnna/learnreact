import React from 'react';
import style from './User.module.css'
import userPhoto from './../../../assets/images/avatar.png'
import { NavLink } from 'react-router-dom';

const User = (props) => {
  const buttonName = () => props.user.followed ? 'Unfollow' : 'Follow';
  const sendID = () => {
    props.getID(props.user.id);
  }
  return (
    <li className={style.user}>
      <NavLink to={'/profile/' + props.user.id}>
        <img className={style.avatar} src={props.user.photos.small !== null? props.user.photos.small : userPhoto} alt="avatar"/>
      </NavLink>
      <div className={style.userInfo}>
        <h4>{props.user.name}</h4>
        <p>{props.user.status}</p>
      </div>
      <div className={style.userLocation}>
        <p>'props.user.location.city'</p>
        <p>'props.user.location.country'</p>
      </div>
      <button className={style.follow} onClick={sendID} disabled={props.disabledButtons.some(id => id === props.user.id)}>{buttonName()}</button>
    </li>
  )
}

export default User;
