import React from 'react';
import { connect } from 'react-redux';
import { requestUsers, setCurrentPage, setFriendship } from '../../data/users-reducer';
import Users from './Users';
import Loader from '../common/Loader/Loader';
import authRedirect from '../../hoc/authRedirect';
import { compose } from 'redux';
import {
  getCurrentPage, getDisabledButtons,
  getIsDisabled,
  getIsFetching,
  getPageSize,
  getTotalUsersCount, getUsers
} from '../../data/selectors/users-selector';

class UsersContainer extends React.Component {

  componentDidMount() {
    this.props.requestUsers(this.props.currentPage, this.props.pageSize);
  }

  onPageChanged = (page) => {
    this.props.setCurrentPage(page);
    this.props.requestUsers(page, this.props.pageSize);
  }

  switchPage = (key) => {
    let currentPage = this.props.currentPage + key;
    currentPage = currentPage < 1 ? 1 : currentPage > Math.ceil(this.props.totalUsersCount / this.props.pageSize) ?
      Math.ceil(this.props.totalUsersCount / this.props.pageSize) : currentPage;
    this.props.setCurrentPage(currentPage);
    this.props.requestUsers(currentPage, this.props.pageSize);
  }

  getID = (userID) => {
    this.props.setFriendship(userID);
  };

  render() {
    return <>
      {this.props.isFetching ? <Loader/> : null}
      <Users
        state={this.props.state}
        getID={this.getID}
        disabledButtons={this.props.disabledButtons}
        pagesCount={Math.ceil(this.props.totalUsersCount / this.props.pageSize)}
        currentPage={this.props.currentPage}
        switchPage={this.switchPage}
        onPageChanged={this.onPageChanged}
      />
    </>
  }
}

const mapStateToProps = (state) => {
  return {
    state: getUsers(state),
    pageSize: getPageSize(state),
    totalUsersCount: getTotalUsersCount(state),
    currentPage: getCurrentPage(state),
    isFetching: getIsFetching(state),
    isDisabled: getIsDisabled(state),
    disabledButtons: getDisabledButtons(state),
  }
};

export default compose(
  connect(mapStateToProps,
    {
      setCurrentPage,
      setFriendship,
      requestUsers
    }),
  authRedirect
)(UsersContainer);
