import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Textarea } from "../common/FormsControl/FormsControl";
import { permittedLength, required } from '../../utils/validators/validators';

const maxLength = permittedLength(50)

const NewDialog = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <Field name='newMessage' component={Textarea} rows={3} validate={[required, maxLength]}/>
      <button>Send</button>
    </form>
  )
}

export const NewDialogContainer = reduxForm({
  form: 'newDialog'
})(NewDialog)
