import React from 'react';
import style from './Dialogs.module.css'
import DialogItem from './DialogItem/DialogItem';
import Message from './Message/Message';
import { NewDialogContainer } from "./NewDialog";

const Dialogs = (props) => {
  let dialogElements = props.state.dialogs.map(dialog => <DialogItem key={dialog.id} state={dialog}/>)
  let messagesElements = props.state.messagesData.map(message => <Message key={message.id} text={message.text}/>)

  let getAnswer = (formData) => {
    props.getAnswer(formData.newMessage);
  }

  return (
    <div>
      <div className={style.dialogs}>
        <div>
          <h2>Dialogs</h2>
          <ul>
            {dialogElements}
          </ul>
        </div>
        <div>
          <ul>
            {messagesElements}
          </ul>
        </div>
      </div>
      <div>
        <NewDialogContainer onSubmit={getAnswer}/>
      </div>
    </div>
  )
}

export default Dialogs;
