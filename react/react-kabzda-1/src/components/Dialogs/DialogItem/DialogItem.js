import React from "react";
import style from './DialogItem.module.css'
import { NavLink } from 'react-router-dom'

const DialogItem = (props) => {
  return (
    <li className={style.dialog}>
      <img className={style.avatar} src={props.state.img} alt="avatar"/>
      <NavLink to={'/dialogs/' + props.state.id}>{props.state.name}</NavLink>
    </li>
  )
}

export default DialogItem;
