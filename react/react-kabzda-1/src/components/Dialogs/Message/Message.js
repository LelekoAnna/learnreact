import React from "react";

const Message = (props) => {
  return (
    <li className='message'>{props.text}</li>
  )
}

export default Message;
