import React from 'react';
import { getAnswer } from '../../data/dialogs-reducer';
import Dialogs from './Dialogs';
import { connect } from 'react-redux';
import authRedirect from '../../hoc/authRedirect';
import { compose } from "redux";

const mapStateToProps = (state) => {
  return {
    state: state.dialogsPage
  }
}

export default compose(
  connect(mapStateToProps, { getAnswer }),
  authRedirect
)(Dialogs);
