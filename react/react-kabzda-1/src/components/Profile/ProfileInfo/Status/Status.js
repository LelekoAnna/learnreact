import React, { useEffect, useState } from 'react';

const Status = (props) => {
  let [ editMode, setEditMode ] = useState(false);
  let [ status, setStatus ] = useState(props.status);

  useEffect(() => {setStatus(props.status)}, [props.status])

  const onMode = () => {
    setEditMode(true);
  }

  const offMode = () => {
    setEditMode(false)
    props.updateStatus(status)
  }

  const updateStatus = (event) => {
    setStatus(event.currentTarget.value)
  }

  return (
    <>
      {editMode ?
        <input autoFocus={true} onBlur={offMode} value={status} onChange={updateStatus}/> :
        <p onDoubleClick={onMode}><small>{props.status}</small></p>}
    </>
  )
}

export default Status;
