import React from 'react';
import style from './ProfileInfo.module.css';
import Loader from '../../common/Loader/Loader';
import Contacts from './Contacts/Contacts';
import Status from "./Status/Status";

const ProfileInfo = (props) => {
  if (!props.profile) {
    return <Loader/>;
  }
  let contactElement;
  if (props.profile.contacts) {
    let socialNetworks = props.profile.contacts;
    let contacts = [];
    for (let contact in socialNetworks) {
      if (socialNetworks[contact]) {
        contacts.push({ network: contact, address: socialNetworks[contact] })
      }
    }
    contactElement = contacts.map((contact, index) => <Contacts key={index} contact={contact}/>)
  }

  return (
    <div className={style.user}>
      <img className={style.avatar} alt='avatar'
           src={props.profile.photos.small}/>
      <div>
        <h1 className={style.userName}>{props.profile.fullName}</h1>
        <Status status={props.status} updateStatus={props.updateStatus}/>
        <p>About me: {props.profile.aboutMe}</p>
        <h3>Contacts:</h3>
        <ul>
          {contactElement}
        </ul>
        {props.profile.lookingForAJob ? <p>Job description: {props.profile.lookingForAJobDescription}</p> : null}
      </div>
    </div>
  )

}

export default ProfileInfo;
