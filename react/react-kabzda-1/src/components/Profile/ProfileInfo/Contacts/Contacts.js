import React from 'react';
import style from './Contacts.module.css'

const Contacts = (props) => {
  return (
    <li>
      <a href={props.contact.address} className={style.contact}>{props.contact.network}</a>
    </li>
  )
}

export default Contacts;
