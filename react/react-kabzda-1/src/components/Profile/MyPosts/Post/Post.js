import React from 'react';
import style from './Post.module.css';

const Post = (props) => {
  return (
    <li className={style.post}>
      <img src='' alt='avatar'/>
      <p>{props.message}</p>
      <span>like ({props.quantityLike})</span>
    </li>
  )
}

export default Post;
