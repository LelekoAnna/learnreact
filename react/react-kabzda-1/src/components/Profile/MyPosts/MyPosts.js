import React from 'react';
import style from './MyPosts.module.css';
import Post from './Post/Post';
import { NewPostFormContainer } from '../NewPostForm';

const MyPosts = props => {
  let postElements = [...props.state].reverse().map(post => <Post key={post.id} message={post.text} quantityLike={post.like}/>)

  let addPost = (formData) => {
    props.addPostActionCreator(formData.newPost);
  }

  return (
    <div>
      <h2>My posts</h2>
      <div className={style.newPost}>
        <NewPostFormContainer onSubmit={addPost}/>
      </div>
      <ul>
        {postElements}
      </ul>
    </div>
  )
}

export default MyPosts;
