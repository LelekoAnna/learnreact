import React from 'react';
import MyPosts from './MyPosts';
import { addPostActionCreator } from '../../../data/profile-reducer';
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    state: state.profilePage.postData
  }
};

const MyPostsContainer = connect (mapStateToProps, { addPostActionCreator }) (MyPosts);

export default MyPostsContainer;
