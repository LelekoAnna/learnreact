import React from 'react';
import style from './MyPosts/MyPosts.module.css';
import { Field, reduxForm } from 'redux-form';
import { permittedLength, required } from '../../utils/validators/validators';
import { Textarea } from '../common/FormsControl/FormsControl';

const maxLength = permittedLength(100)

const NewPostForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <Field name='newPost' component={Textarea} rows={3} className={style.post} validate={[required, maxLength]} placeholder='Enter new post'/>
      <button className={style.button}>Add post</button>
    </form>
  )
}

export const NewPostFormContainer = reduxForm({
  form: 'newPost'
})(NewPostForm)
