import React from 'react';
import style from './Header.module.css';
import { NavLink } from "react-router-dom";

const Header = (props) => {
  return (
    <header className={style.header}>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Logo_TV_2015.svg/1200px-Logo_TV_2015.svg.png" alt="logo"/>
      <div>
        {props.isAuth ? <div className={style.auth}><p>{props.login}</p><button onClick={props.logout}>LOGOUT</button></div> : <NavLink to='/login'>LOGIN</NavLink>}
      </div>
    </header>
  )
}

export default Header;
