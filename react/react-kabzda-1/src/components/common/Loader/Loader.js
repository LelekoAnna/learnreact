import React from "react";
import style from './Loader.module.css'

const Loader =() => {
  return (
    <div className={style.circle}>
      <div />
    </div>
  )
}

export default Loader;
