import React from 'react';
import style from './FormsControle.module.css'

export const Textarea = ({input, meta, ...props}) => {
  const isError = meta.touched && meta.error;
  return (
  <div className={isError? style.error: ''}>
    <textarea {...input} {...props}/>
    {isError && <span>{meta.error}</span>}
  </div>
  )
}

export const Input = ({input, meta, ...props}) => {
  const isError = meta.touched && meta.error;
  return (
    <div className={isError? style.error: ''}>
      <input {...input} {...props}/>
      {isError && <span>{meta.error}</span>}
    </div>
  )
}
