import React from 'react';
import style from './Paginator.module.css'

const Paginator = (props) => {
  let pages = [];
  if (props.pagesCount <= 7) {
    for (let i = 1; i <= props.pagesCount; i++) {
      pages.push(i);
    }
  } else if (props.currentPage < 5) {
    pages = [ 1, 2, 3, 4, 5, '...', props.pagesCount ]
  } else if (props.currentPage > props.pagesCount - 5) {
    pages = [ 1, '...', props.pagesCount - 5, props.pagesCount - 4, props.pagesCount - 3, props.pagesCount - 2, props.pagesCount - 1, props.pagesCount ]
  } else {
    pages = [ 1, '...', props.currentPage - 1, props.currentPage, props.currentPage + 1, '...', props.pagesCount ]
  }

  return (
      <div>
        <button onClick={() => {props.switchPage(-1)}}>Prev</button>
        {pages.map((page, index) => <button key={index}
                                            className={props.currentPage === page ? style.active : undefined}
                                            onClick={() => {props.onPageChanged(page)}}>{page}</button>)}
        <button onClick={() => {props.switchPage(1)}}>Next</button>
      </div>
  )
}

export default Paginator;
