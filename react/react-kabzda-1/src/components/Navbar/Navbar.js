import React from 'react';
import style from './Navbar.module.css';
import NavbarItem from "./NavbarItem/NavbarItem";

const Navbar = (props) => {
  let navbarElements = props.state.pages.map(page => <NavbarItem key={page.id} data={page} friends={props.state.friends}/>)
  return (
    <nav className={style.nav}>
      <ul>
        {navbarElements}
      </ul>
    </nav>
  )
}

export default Navbar;
