import React from 'react';
import style from './NavbarItem.module.css';
import { NavLink } from 'react-router-dom';

const NavbarItem = (props) => {
  if (props.data.page === 'Friends') {
    let friendsElement = props.friends.map(friend => (
        <div key={friend.id}>
          <img className={style.avatar} src={friend.img} alt={friend.name}/>
          <p>{friend.name}</p>
        </div>
      )
    )
    return (
      <li className={style.items}>
        <NavLink to={props.data.url} activeClassName={style.active}>{props.data.page}</NavLink>
        <div className={style.friend}>
          {friendsElement}
        </div>
      </li>
    )
  } else {
    return (
      <li className={style.items}>
        <NavLink to={props.data.url} activeClassName={style.active}>{props.data.page}</NavLink>
      </li>
    )
  }
}

export default NavbarItem;
