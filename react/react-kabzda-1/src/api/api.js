import * as axios from 'axios';

const instance = axios.create({
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  withCredentials: true,
  headers: {
    'API-KEY': 'e0f3a827-bcee-4d7c-9ca6-c754315c84ae'
  }
});

export const usersAPI = {
  getUsers (currentPage = 1, pageSize = 5) {
    return instance.get(`users?page=${currentPage}&count=${pageSize}`).then(response => response.data)
  }
};

export const followAPI = {
  isFriend (userID) {
    return instance.get('/follow/' + userID).then(response => response.data)
  },
  follow (userID) {
    return instance.post('follow/' + userID).then(response => response.data);
  },
  unFollow (userID) {
    return instance.delete('follow/' + userID).then(response => response.data);
  }
};

export const profileAPI = {
  getProfile (userID) {
    return instance.get('profile/' + userID).then(response => response.data)
  },
  getStatus (userID) {
    return instance.get('profile/status/' + userID).then(response => response.data)
  },
  updateStatus (status) {
    return instance.put('profile/status', {status}).then(response => response.data)
  }
};

export const authAPI = {
  authMe () {
    return instance.get('auth/me').then(response => response.data)
  },
  login (email, password, rememberMe=false) {
    return instance.post('auth/login', {email, password, rememberMe}).then(response => response.data)
  },
  logout () {
    return instance.delete('auth/login').then(response => response.data);
  }
}

