import { profileAPI } from '../api/api';

const ADD_POST = 'profile/ADD-POST';
const SET_USER_PROFILE = 'profile/SET_USER_PROFILE';
const SET_USER_STATUS = 'profile/SET_USER_STATUS';

let initialState = {
  postData:
    [
      { id: 1, text: 'Hi! How are you', like: 5 },
      { id: 2, text: 'It is my first post', like: 3 }
    ],
  profile: null,
  status: ''
}

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST:
      let newPost = {
        id: state.postData.length + 1,
        text: action.postMessage,
        like: 0
      }
      return { ...state, postData: [ ...state.postData, newPost ] };
    case SET_USER_PROFILE:
      return { ...state, profile: action.profile };
    case SET_USER_STATUS:
      return { ...state, status: action.status };
    default:
      return state;
  }
}

export const addPostActionCreator = (postMessage) => ({ type: ADD_POST, postMessage });
export const setUserProfile = (profile) => ({ type: SET_USER_PROFILE, profile })
export const setUserStatus = (status) => ({ type: SET_USER_STATUS, status })

export const getProfile = (userID) => async (dispatch) => {
  let response = await profileAPI.getProfile(userID)
  dispatch(setUserProfile(response));
}

export const getStatus = (userID) => async (dispatch) => {
  let response = await profileAPI.getStatus(userID)
  dispatch(setUserStatus(response));
}

export const updateStatus = (status) => async (dispatch) => {
  let response = await profileAPI.updateStatus(status)
  if (response.resultCode === 0) {
    dispatch(setUserStatus(status));
  }
}

export default profileReducer;
