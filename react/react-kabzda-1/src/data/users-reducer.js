import { followAPI, profileAPI, usersAPI } from '../api/api';

const FRIENDSHIP = 'users/FRIENDSHIP';
const SET_USERS = 'users/SET_USERS';
const SET_CURRENT_PAGE = 'users/SET_CURRENT_PAGE';
const SET_TOTAL_USERS_COUNT = 'users/SET_TOTAL_USERS_COUNT';
const TOGGLE_IS_FETCHING = 'users/TOGGLE_IS_FETCHING';
const TOGGLE_DISABLED = 'users/TOGGLE_DISABLED';

let initialState = {
  users: [],
  pageSize: 5,
  totalUsersCount: 0,
  currentPage: 1,
  isFetching: false,
  isDisabled: false,
  disabledButtons: []
};

const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FRIENDSHIP:
      return {
        ...state,
        users: state.users.map(user => user.id === action.userID ? { ...user, followed: !user.followed } : user)
      };
    case SET_USERS:
      return { ...state, users: action.users };
    case SET_TOTAL_USERS_COUNT:
      return { ...state, totalUsersCount: action.total };
    case SET_CURRENT_PAGE:
      return { ...state, currentPage: action.currentPage };
    case TOGGLE_IS_FETCHING:
      return { ...state, isFetching: action.isFetching };
    case TOGGLE_DISABLED:
      return {
        ...state,
        isDisabled: action.isDisabled,
        disabledButtons: action.isDisabled ? [ ...state.disabledButtons, action.userID ] : state.disabledButtons.filter(id => id !== action.userID)
      }
    default:
      return state;
  }
}

export const friendship = (userID) => ({ type: FRIENDSHIP, userID });
export const setUsers = (users) => ({ type: SET_USERS, users });
export const setCurrentPage = (currentPage) => ({ type: SET_CURRENT_PAGE, currentPage });
export const setTotalUsersCount = (total) => ({ type: SET_TOTAL_USERS_COUNT, total });
export const toggleFetching = (isFetching) => ({ type: TOGGLE_IS_FETCHING, isFetching });
export const setDisableToButton = (isDisabled, userID) => ({ type: TOGGLE_DISABLED, isDisabled, userID })

export const requestUsers = (currentPage, pageSize) => async (dispatch) => {
  dispatch(toggleFetching(true));
  let response = await usersAPI.getUsers(currentPage, pageSize)
  dispatch(setUsers(response.items));
  dispatch(setTotalUsersCount(response.totalCount));
  dispatch(toggleFetching(false));
}

const followUnfollowFlow = async (dispatch, userID, apiMethod) => {
  let response = await apiMethod(userID);
      if (response.resultCode === 0) {
        dispatch(friendship(userID));
      }
      dispatch(setDisableToButton(false, userID));
}

export const setFriendship = (userID) => async (dispatch) => {
  dispatch(setDisableToButton(true, userID));
  let response = await followAPI.isFriend(userID)
  if (!response) {
    followUnfollowFlow(dispatch, userID, followAPI.follow)
  } else {
    followUnfollowFlow(dispatch, userID, followAPI.unFollow)
  }
}

export default usersReducer;
