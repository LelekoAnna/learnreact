import { authAPI } from '../api/api';
import { stopSubmit } from 'redux-form';

const SET_USER_DATA = 'auth/SET_USER_DATA';

let initialState = {
  id: null,
  login: null,
  email: null,
  isAuth: false
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return { ...state, ...action.data };
    default:
      return state;
  }
}

export const setUserAuthData = (id, login, email, isAuth) => ({
  type: SET_USER_DATA,
  data: { id, login, email, isAuth }
})
export const authMe = () => async (dispatch) => {
  let response = await authAPI.authMe();
  if (response.resultCode === 0) {
    let { id, login, email } = response.data;
    dispatch(setUserAuthData(id, login, email, true));
  }
}

export const login = (email, password, rememberMe) => async (dispatch) => {
  let response = await authAPI.login(email, password, rememberMe);
  if (response.resultCode === 0) {
    dispatch(authMe());
  } else {
    const message = response.messages.length ? response.messages[0] : 'Some error';
    dispatch(stopSubmit('login', { _error: message }))
  }
}

export const logout = () => async (dispatch) => {
  let response = await authAPI.logout();
  if (response.resultCode === 0) {
    dispatch(setUserAuthData(null, null, null, false));
  }
}

export default authReducer;
