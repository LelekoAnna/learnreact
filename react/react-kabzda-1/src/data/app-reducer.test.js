import appReducer, { initializedSuccess } from './app-reducer';

let state = {
  initialized: false
}

it('should return true if initialized success', () => {
  let action = initializedSuccess();
  let newState = appReducer(state, action);

  expect(newState.initialized).toBe(true);

});
