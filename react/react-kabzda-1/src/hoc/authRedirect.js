import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.isAuth
  }
}

const authRedirect = (Component) => {
  let ContainerComponent = (props) => {
    if (!props.isAuth) {
      return <Redirect to='/login'/>
    }
    return <Component {...props}/>
  }
  ContainerComponent = connect(mapStateToProps)(ContainerComponent)
  return ContainerComponent;
}

export default authRedirect;
