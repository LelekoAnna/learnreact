export const required = value => value? undefined: 'Field is required';

export const permittedLength = (maxLength) => value => value.length <= maxLength ? undefined: `Max length is ${maxLength} symbols`
